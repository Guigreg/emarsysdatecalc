from calendar import weekday
from datetime import datetime, time, timedelta

# date should be in format 'Jun 6 2005 1:33PM' (monday) and turnaroundTime should be in hours
import math

FRIDAY = 4
NUM_OF_WORKDAYS = 5
WEEKEND_DAYS = 2
WORKDAYS = range(0, 5)
ONE_DAY_WORKING_HOURS = 8
START_OF_WORKDAY = 9
END_OF_WORKDAY = 17


def calc_due_date(submit_date, turnaround_time):
    formatted_date = format_date(submit_date)

    check_working_hours(formatted_date)
    check_turnaround_time(turnaround_time)

    return calculate(formatted_date, turnaround_time)


def calculate(date, turnaround_time):
    days = int(turnaround_time / ONE_DAY_WORKING_HOURS)
    hours = turnaround_time % ONE_DAY_WORKING_HOURS

    date = add_hours(date, hours)
    date = add_days(date, days)
    return date


def add_days(date, days):
    weeks = int(days / NUM_OF_WORKDAYS)
    remaining_days = days % NUM_OF_WORKDAYS

    if date.weekday() + remaining_days > FRIDAY:
        remaining_days += WEEKEND_DAYS
    date += timedelta(weeks=weeks)
    return date + timedelta(days=remaining_days)


def add_hours(date, hours):
    if date.hour + hours >= END_OF_WORKDAY:
        remaining_hours = int(math.fabs(END_OF_WORKDAY - (date.hour + hours)))
        end_hours = START_OF_WORKDAY + remaining_hours

        date = date.replace(hour=end_hours)

        if date.weekday() == FRIDAY:
            date += timedelta(days=WEEKEND_DAYS)
        return date + timedelta(days=1)
    else:
        return date + timedelta(hours=hours)


def check_working_hours(date):
    if not time(START_OF_WORKDAY) <= date.time() <= time(END_OF_WORKDAY) or date.weekday() not in WORKDAYS:
        raise ValueError('Date is not in workdays between 9 and 17')


def check_turnaround_time(turnaround_time):
    if isinstance(turnaround_time, int):
        return turnaround_time
    else:
        raise ValueError('TurnaroundTime is in an incorrect form. Expected: "15" like this many hours')


def format_date(date):
    try:
        return datetime.strptime(date, '%b %d %Y %I:%M%p')
    except ValueError as e:
        raise ValueError('Error happened during date parsing: %s', e)
