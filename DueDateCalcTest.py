import unittest
from datetime import time, datetime

from DueDateCalc import calc_due_date


class DueDateCalcTest(unittest.TestCase):
    def test_throw_late(self):
        with self.assertRaises(ValueError):
            calc_due_date('Jun 21 2017 5:01PM', 6)

    def test_throw_early(self):
        with self.assertRaises(ValueError):
            calc_due_date('Jun 21 2017 8:59AM', 6)

    def test_throw_not_workday(self):
        with self.assertRaises(ValueError):
            calc_due_date('Jun 18 2017 2:15PM', 6)

    def test_throw_not_workday2(self):
        with self.assertRaises(ValueError):
            calc_due_date('Jun 17 2017 2:15PM', 6)

    def test_throw_incorrect_format(self):
        with self.assertRaises(ValueError):
            calc_due_date('21 Jun 2017 2:00PM', 6)

    def test_throw_incorrect_format2(self):
        with self.assertRaises(ValueError):
            calc_due_date('Jun 21 2017 2:00PM', 1.2)

    def test_throw_incorrect_format3(self):
        with self.assertRaises(ValueError):
            calc_due_date('Jun 21 2017 2:00PM', 'alma')

    def test_simple_hour_adding(self):
        self.assertEqual(calc_due_date('Jun 21 2017 2:00PM', 2).time(), time(16))

    def test_workday_skip(self):
        self.assertEqual(calc_due_date('Jun 21 2017 2:17PM', 3), self.create_datetime('Jun 22 2017 9:17AM'))

    def test_weekend_skip(self):
        self.assertEqual(calc_due_date('Jun 23 2017 2:17PM', 3), self.create_datetime('Jun 26 2017 9:17AM'))

    def test_day_addition(self):
        self.assertEqual(calc_due_date('Jun 22 2017 2:17PM', 8), self.create_datetime('Jun 23 2017 2:17PM'))

    def test_week_addition(self):
        self.assertEqual(calc_due_date('Jun 22 2017 2:17PM', 40), self.create_datetime('Jun 29 2017 2:17PM'))

    def test_everything(self):
        self.assertEqual(calc_due_date('Jun 23 2017 2:17PM', 43), self.create_datetime('Jul 3 2017 9:17AM'))

    def create_datetime(self, _date):
        return datetime.strptime(_date, '%b %d %Y %I:%M%p')
if __name__ == '__main__':
    unittest.main()